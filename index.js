/*
Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)
*/
let trainer = {
    name: "",
    age: 0,
    pokemon: [],
    friends: {
      hoenn: [],
      kanto: []
    },
    // Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
    talk: function() {
      console.log("Pikachu! I choose you!");
    }
  };

//   6. Access the trainer object properties using dot and square bracket notation.
trainer.name = "Ash";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends ={
    hoenn: ['May', 'Max'],
    kanto: ['Brock', 'Misty']
}

// 7. Invoke/call the trainer talk object method.
trainer.talk();

// 8. Create a constructor for creating a pokemon with the following properties:
// - Name (Provided as an argument to the contructor)
// - Level (Provided as an argument to the contructor)
// - Health (Create an equation that uses the level property)
// - Attack (Create an equation that uses the level property)

function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;

  this.tackle = function(target) {

    console.log( this.name + " tackled " +target.name);
    target.health -= this.attack;
    console.log( target.name + "'s health is now reduced to "+target.health);
    if (target.health <= 0) {
        target.faint()
    }
}
}

let pikachuName = new Pokemon('Pikachu', 10);
let geodudeName = new Pokemon('Geodude', 8);
let MewtwoName = new Pokemon("Mewtwo", 100);

console.log(pikachuName);
console.log(geodudeName);
console.log(MewtwoName );

geodudeName.tackle(pikachuName);
console.log(pikachuName);

MewtwoName.tackle(geodudeName);
console.log(geodudeName);